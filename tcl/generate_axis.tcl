##################
## CCN PACK IPS ##
##################
set ProjectDirPath [file join ${::fwfwk::PrjBuildPath} ${::fwfwk::PrjBuildName}]


# GENERATE AXIS REGISTER SLICE PACKET
set ipPath ${ProjectDirPath}.srcs/sources_1/ip/ccn_axis_reg_packet/
set checkFile [::fwfwk::utils::findFiles ${ipPath} "*.xci"]
set lCheck [llength $test]
if {$lCheck == } {
    set xcipath [create_ip -name axis_register_slice -vendor xilinx.com -library ip -version 1.1 -module_name ccn_axis_reg_packet]

    set_property -dict [list \
            CONFIG.HAS_ACLKEN 0 \
            CONFIG.HAS_TKEEP 0 \
            CONFIG.HAS_TLAST 0 \
            CONFIG.HAS_TREADY 1 \
            CONFIG.HAS_TSTRB 0 \
            CONFIG.REG_CONFIG 7 \
            CONFIG.TDATA_NUM_BYTES [expr $CCN_DPKT_W/8] \
            CONFIG.TDEST_WIDTH 0 \
            CONFIG.TID_WIDTH 0 \
            CONFIG.TUSER_WIDTH $CCN_DPKT_TU_W \
        ] [get_ips ccn_axis_reg_packet]

    set_property GENERATE_SYNTH_CHECKPOINT 0 [get_files $xcipath]
} else {
    $::fwfwk::printInfo "INFO: '$ipPath' already created. Skipping creation..."
}




# GENERATE AXIS WIDTH CONV PACKET TO FRAME
# set moduleNameWC "ccn_axis_wconv_packet_frame_$IP_NAME"
set ipPath ${ProjectDirPath}.srcs/sources_1/ip/ccn_axis_wconv_packet_frame/
set checkFile [::fwfwk::utils::findFiles ${ipPath} "*.xci"]
set lCheck [llength $test]
if {$lCheck == } {
    set xcipath [create_ip -name axis_dwidth_converter -vendor xilinx.com -library ip -version 1.1 -module_name ccn_axis_wconv_packet_frame]

    set_property -dict [list \
        CONFIG.S_TDATA_NUM_BYTES [expr $CCN_DPKT_W/8] \
        CONFIG.M_TDATA_NUM_BYTES [expr $CCN_FRAME_W/8] \
        CONFIG.HAS_TLAST {1} \
        CONFIG.TUSER_BITS_PER_BYTE {1}\
        ] [get_ips ccn_axis_wconv_packet_frame]

    set_property GENERATE_SYNTH_CHECKPOINT 0 [get_files $xcipath]
} else {
    ::fwfwk::printInfo "INFO: '${::fwfwk::PrjBuildPath}/${::fwfwk::PrjBuildName}.srcs/sources_1/ip/${ccn_axis_wconv_packet_frame}/${ccn_axis_wconv_packet_frame}.xci' already created. Skipping creation"
}


# GENERATE AXI INTERCONNECT
# set moduleNameAI "ccn_axis_header_interco_$IP_NAME"
set ipPath ${ProjectDirPath}.srcs/sources_1/ip/ccn_axis_header_interco/
set checkFile [::fwfwk::utils::findFiles ${ipPath} "*.xci"]
set lCheck [llength $test]
if {$lCheck == } {
    set xcipath [create_ip -name axis_interconnect -vendor xilinx.com -library ip -version 1.1 -module_name ccn_axis_header_interco]

    # Decide GCD in decreasing power of two
    set GCD 32

    while {[expr !((($CCN_FRAME_W/8%$GCD) == 0) && (($CCN_DPKT_W/8%$GCD) == 0))]} {
        set GCD [expr $GCD/2]
    }

    set_property -dict [list \
            CONFIG.ARBITER_TYPE {Fixed} \
            CONFIG.C_NUM_SI_SLOTS {2} \
            CONFIG.C_NUM_MI_SLOTS {1} \
            CONFIG.C_M00_AXIS_IS_ACLK_ASYNC 0 \
            CONFIG.C_M00_AXIS_REG_CONFIG 1 \
            CONFIG.C_S00_AXIS_IS_ACLK_ASYNC 0 \
            CONFIG.C_S00_AXIS_REG_CONFIG 0 \
            CONFIG.C_S01_AXIS_IS_ACLK_ASYNC 0 \
            CONFIG.C_S01_AXIS_REG_CONFIG 1 \
            CONFIG.C_SWITCH_MAX_XFERS_PER_ARB 0 \
            CONFIG.C_SWITCH_MI_REG_CONFIG 0 \
            CONFIG.C_SWITCH_NUM_CYCLES_TIMEOUT 1 \
            CONFIG.C_SWITCH_SI_REG_CONFIG 1 \
            CONFIG.HAS_TDATA true \
            CONFIG.HAS_TDEST false \
            CONFIG.HAS_TID false \
            CONFIG.HAS_TKEEP true \
            CONFIG.HAS_TLAST true \
            CONFIG.HAS_TSTRB false \
            CONFIG.HAS_TUSER true \
            CONFIG.M00_AXIS_FIFO_MODE {0_(Disabled)} \
            CONFIG.M00_AXIS_TDATA_NUM_BYTES [expr $CCN_FRAME_W/8] \
            CONFIG.M00_S00_CONNECTIVITY true \
            CONFIG.M00_S01_CONNECTIVITY true \
            CONFIG.S00_AXIS_FIFO_MODE {0_(Disabled)} \
            CONFIG.S00_AXIS_TDATA_NUM_BYTES [expr $CCN_FRAME_HEADER_W/8]  \
            CONFIG.S01_AXIS_FIFO_MODE {0_(Disabled)} \
            CONFIG.S01_AXIS_TDATA_NUM_BYTES [expr $CCN_DPKT_W/8]  \
            CONFIG.SWITCH_TDATA_NUM_BYTES 4  \
            CONFIG.SWITCH_PACKET_MODE true  \
            CONFIG.SWITCH_USE_ACLKEN false  \
            CONFIG.SYNCHRONIZATION_STAGES 2  \
        ] [get_ips ccn_axis_header_interco]

    # Unused properties
            #CONFIG.C_M00_AXIS_FIFO_DEPTH {2048} \
            #CONFIG.C_S00_AXIS_ACLK_RATIO 12 \
            #CONFIG.C_S00_AXIS_FIFO_DEPTH 32 \
            #CONFIG.C_S01_AXIS_ACLK_RATIO 12 \
            #CONFIG.C_S01_AXIS_FIFO_DEPTH 32 \
            #CONFIG.C_M00_AXIS_ACLK_RATIO 12 \
            #CONFIG.C_M00_AXIS_BASETDEST 0x00000000 \
            #CONFIG.C_M00_AXIS_HIGHTDEST 0x00000000 \
            #CONFIG.C_SWITCH_TID_WIDTH 1 \
            #CONFIG.C_SWITCH_TDEST_WIDTH 1 \
            #CONFIG.SWITCH_TUSER_BITS_PER_BYTE 1 \

    set_property GENERATE_SYNTH_CHECKPOINT 0 [get_files $xcipath]

} else {
    ::fwfwk::printInfo "INFO: '${::fwfwk::PrjBuildPath}/${::fwfwk::PrjBuildName}.srcs/sources_1/ip/${ccn_axis_header_interco}/${ccn_axis_header_interco}.xci' already created. Skipping creation"
}



# GENERATE AXIS FRAME FIFO
#set moduleNameAF "ccn_axis_fifo_pframe_$IP_NAME"
#set xcipath [create_ip -name axis_data_fifo -vendor xilinx.com -library ip -version 2.0 -module_name ccn_axis_fifo_pframe]

#set_property -dict [list \
#    CONFIG.TDATA_NUM_BYTES [expr $CCN_FRAME_W/8] \
#    CONFIG.TUSER_WIDTH {1} \
#    CONFIG.FIFO_DEPTH {256} \
#    CONFIG.FIFO_MODE {2} \
#    CONFIG.IS_ACLK_ASYNC {1} \
#    CONFIG.SYNCHRONIZATION_STAGES {3} \
#    CONFIG.HAS_TKEEP {1} \
#    CONFIG.HAS_TLAST {1} \
#    CONFIG.HAS_PROG_EMPTY {0} \
#    ] [get_ips ccn_axis_fifo_pframe]

#set_property GENERATE_SYNTH_CHECKPOINT 0 [get_files $xcipath]

####################
## CCN UNPACK IPS ##
####################

# GENERATE AXIS WIDTH CONV FRAME TO PACKET

set ipPath ${ProjectDirPath}.srcs/sources_1/ip/ccn_axis_wconv_frame_packet/
set checkFile [::fwfwk::utils::findFiles ${ipPath} "*.xci"]
set lCheck [llength $test]
if {$lCheck == } {
    set xcipath [create_ip -name axis_dwidth_converter -vendor xilinx.com -library ip -version 1.1 -module_name ccn_axis_wconv_frame_packet]

    set_property -dict [list \
        CONFIG.S_TDATA_NUM_BYTES [expr $CCN_FRAME_W/8] \
        CONFIG.M_TDATA_NUM_BYTES [expr $CCN_UPKT_W/8] \
        CONFIG.HAS_TLAST {1} \
        CONFIG.HAS_TKEEP {1} \
        CONFIG.TUSER_BITS_PER_BYTE {0}\
        ] [get_ips ccn_axis_wconv_frame_packet]

    set_property GENERATE_SYNTH_CHECKPOINT 0 [get_files $xcipath]
} else {
    ::fwfwk::printInfo "INFO: '${::fwfwk::PrjBuildPath}/${::fwfwk::PrjBuildName}.srcs/sources_1/ip/${ccn_axis_wconv_frame_packet}/${ccn_axis_wconv_frame_packet}.xci' already created. Skipping creation"
}


# GENERATE AXIS FRAME FIFO
set ipPath ${ProjectDirPath}.srcs/sources_1/ip/ccn_axis_wconv_frame_packet/
set checkFile [::fwfwk::utils::findFiles ${ipPath} "*.xci"]
set lCheck [llength $test]
if {$lCheck == } {

    set xcipath [create_ip -name axis_data_fifo -vendor xilinx.com -library ip -version 2.0 -module_name ccn_axis_fifo_frame]

    set_property -dict [list \
        CONFIG.TDATA_NUM_BYTES [expr $CCN_FRAME_W/8] \
        CONFIG.TUSER_WIDTH {1} \
        CONFIG.FIFO_DEPTH {64} \
        CONFIG.FIFO_MODE {1} \
        CONFIG.IS_ACLK_ASYNC {1} \
        CONFIG.SYNCHRONIZATION_STAGES {3} \
        CONFIG.HAS_TKEEP {1} \
        CONFIG.HAS_TLAST {1} \
        CONFIG.HAS_PROG_EMPTY {0} \
        ] [get_ips ccn_axis_fifo_frame]

    set_property GENERATE_SYNTH_CHECKPOINT 0 [get_files $xcipath]
} else {
    ::fwfwk::printInfo "INFO: '${::fwfwk::PrjBuildPath}/${::fwfwk::PrjBuildName}.srcs/sources_1/ip/${ccn_axis_fifo_frame}/${ccn_axis_fifo_frame}.xci' already created. Skipping creation"
}

