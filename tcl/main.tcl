################################################################################
# Main tcl for the module
################################################################################

# ==============================================================================
proc init {} {
    variable CCN_DPKT_W
    variable CCN_DPKT_TU_W
    variable CCN_FRAME_W
    variable CCN_FRAME_HEADER_W
    variable CCN_UPKT_W
    variable CCN_UPKT_TU_W


}

# ==============================================================================
proc setSources {} {
  variable Sources

  # Generate VHDL package with mode version
  genModVerFile VHDL ../hdl/pkg_ccn_packet_version.vhd
  lappend Sources {"../hdl/pkg_ccn_packet_version.vhd" "VHDL" }

  lappend Sources {"../hdl/ccn_pack.vhd"            "VHDL" }
  lappend Sources {"../hdl/ccn_unpack.vhd"          "VHDL" }
  lappend Sources {"../hdl/top_ccn_packeter.vhd"    "VHDL" }
  lappend Sources {"../hdl/top_ccn_unpacketer.vhd"  "VHDL" }

  lappend Sources {"../sim/tb_ccn.vhd"              "VHDL 2008"  "" "simulation"}
  lappend Sources {"../sim/testinput.dat"           ""           "" "simulation"}
  lappend Sources {"../sim/testoutput.dat"          ""           "" "simulation"}
  lappend Sources {"../sim/tb_ccn_global.wcfg"      ""           "" "simulation"}


}

# ==============================================================================
proc setAddressSpace {} {

    variable AddressSpacePack
    addAddressSpace AddressSpacePack "ccn_packeter" RDL {} ../rdl/ccn_packeter.rdl

    variable AddressSpaceUnpack
    addAddressSpace AddressSpaceUnpack "ccn_unpacketer" RDL {} ../rdl/ccn_unpacketer.rdl
}

# ==============================================================================
proc doOnCreate {} {
    variable CCN_DPKT_W
    variable CCN_DPKT_TU_W
    variable CCN_FRAME_W
    variable CCN_FRAME_HEADER_W
    variable CCN_UPKT_W

    variable Sources
    variable Config

    addSources Sources

    set INS_NAME $Config(INS_NAME)
    set ipVendor xilinx.com
    set ipLibrary ip
    set ipName axis_data_fifo
    
    set ipProp [ list CONFIG.TDATA_NUM_BYTES [expr $CCN_FRAME_W/8] \
    CONFIG.TUSER_WIDTH 1\
    CONFIG.FIFO_DEPTH 256\
    CONFIG.FIFO_MODE 2 \
    CONFIG.IS_ACLK_ASYNC 1 \
    CONFIG.SYNCHRONIZATION_STAGES 3\
    CONFIG.HAS_TKEEP 1\
    CONFIG.HAS_TLAST 1\
    CONFIG.HAS_PROG_EMPTY 0
    ]

    fwfwk::tool::wrapIP  "xilinx.com" "ip" "axis_data_fifo" $ipProp $INS_NAME
    set ipPath ${::fwfwk::PrjBuildPath}/${::fwfwk::PrjBuildName}.srcs/sources_1/ip/${INS_NAME}/${INS_NAME}.xci
    set_property GENERATE_SYNTH_CHECKPOINT 0 [get_files $ipPath]

    source generate_axis.tcl
  }




# ==============================================================================
proc doOnBuild {} {
}

# ==============================================================================
proc setSim {} {
    variable SimTop
    set SimTop tb_ccn

}
