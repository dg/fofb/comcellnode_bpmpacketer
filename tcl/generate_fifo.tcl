#######################
## CCN PACK FIFO IP  ##
#######################

# GENERATE AXIS FRAME FIFO
#set moduleNameAF "ccn_axis_fifo_pframe_$IP_NAME"
set xcipath [create_ip -name axis_data_fifo -vendor xilinx.com -library ip -version 2.0 -module_name ccn_axis_fifo_pframe]

set_property -dict [list \
    CONFIG.TDATA_NUM_BYTES [expr $CCN_FRAME_W/8] \
    CONFIG.TUSER_WIDTH {1} \
    CONFIG.FIFO_DEPTH {256} \
    CONFIG.FIFO_MODE {2} \
    CONFIG.IS_ACLK_ASYNC {1} \
    CONFIG.SYNCHRONIZATION_STAGES {3} \
    CONFIG.HAS_TKEEP {1} \
    CONFIG.HAS_TLAST {1} \
    CONFIG.HAS_PROG_EMPTY {0} \
    ] [get_ips ccn_axis_fifo_pframe]

set_property GENERATE_SYNTH_CHECKPOINT 0 [get_files $xcipath]

set ipVendor xilinx.com
set ipLibrary ip
set ipName axis_data_fifo
set ipConfig [list \
    CONFIG.TDATA_NUM_BYTES [expr $CCN_FRAME_W/8] \
    CONFIG.TUSER_WIDTH {1} \
    CONFIG.FIFO_DEPTH {256} \
    CONFIG.FIFO_MODE {2} \
    CONFIG.IS_ACLK_ASYNC {1} \
    CONFIG.SYNCHRONIZATION_STAGES {3} \
    CONFIG.HAS_TKEEP {1} \
    CONFIG.HAS_TLAST {1} \
    CONFIG.HAS_PROG_EMPTY {0} \
    ]
wrapIP  $ipVendor $ipLibrary $ipName $ipConfig $INS_NAME




