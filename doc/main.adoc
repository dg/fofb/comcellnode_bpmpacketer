= COMCELLNODE PACKET module documentation

|===
| Packeter Module ID | 0x507E1743 | (1350440771)
| Unpacketer Module ID | 0x507E1746 | (1350440774)
|===


// ================================================================================
== Description

This module contains blocs to allow communication between FOFB nodes.
It provides a packeter which embeds packets in Ethernet L2 Frames ready to be processed by the Xilinx Ethernet Subsystem.
(Only the Ethernet L2 CRC is missing, which is added by the IP).
Symetrically, it provides a unpacketer, which retrieve data from packets in L2 frames, received from the Xilinx Ethernet Subsystem.
_The Xilinx Ethernet Subsystem IP is included in the module CCN_ETHERNET_

The packeter and unpacketer are generic.
They can be configured to carry position or correction data.
One just has to configure the data stream width.


=== Packeter

It receives a data stream from the application (i.e. from COMBPM module).

The AXIS signals used are:

* TDATA: contains the data, packed format depends on the user.
* TUSER: contains a sequence number.
* TVALID: Data strobe
* TREADY: Data throthling from the module.

The packeter waits for a sequence number change to prepare a new packet.
A known number (indicated by CPU configuration) of data words are included in the output packet.

A header is appended to the transmitted data.
It contains some parts of Ethernet Frame (as expected by Xilinx Ethernet Subsystem), concatenated with the timestamp and protocol ID.

Error are raised when:

- Supplementary data words with same sequence number are received.
- Timeout expecting next data words (incomplete packet).

=== Unpacketer

It receives L2 packets from the Ethernet IP.

It simply removes the header, keeping the values in status register accessible from the AXI-MM.
Then it changes the data width to get data word by data word transfer on the AXIS output.


// ================================================================================
== Overview

=== Packeter

* *AXIS Register Slice*. Used to bufferize the incoming data words.
Gives time to detect a new sequence and handle back pressure from the stream.
Indeed, the register will hold the first data word while the header is transmitted.

* *Detect frame sequence*.
Using the sequence number, it detects changes in it to synchronize the state machine to the begining of a new packet.

* *Pass or Dump*. Dump data words when seeking for a sequence begining.
This handles the AXIS signaling to free the input register.

* *Count BPM packet*. Programmed with the expected number of data words, this decrement when a new data word is added to the Ethernet output frame.

* *Header*. Prepared header to transfer prior the first data word.
It is configured with MAC addresses, Frame length and receive a timestamp when the first data word reaches the input.

* *State Machine*.
Coordinate the interconnexion, pass/dump.
It detects erroneous situation (too many/few data words).

* *Latching sequence number*
This feature was added to compute alignement of the frame sequence number.
Usefull for a park of two kinds of electronics, which cannot be fa seq aligned.

.Basic overview of CCN packeter
image::packeter_overview.svg[]

=== Unpacketer

* *AXIS FIFO*. Used to bufferize the incoming stream and perform CDC.

* *Header removal*. Based on a state machine, it removes the header while keeping the values in register ready to be read by the AXI-MM.
  There it also checks the MAC addressed and protocol ID.

* *AXIS Wconv*. Convert the remaining stream to proper data words.

.Basic overview of CCN unpacketer
image::unpacketer_overview.svg[]

// ================================================================================
== Details

=== Fabricate an Ethernet Frame

When used with the CCN_ETHERNET module, the packeter fabricates a frame described in the following pictures.
This illustration is based on SOLEIL FOFB application where TDATA carry either position or correction data.

.Ethernet Frame fabricated
image::ethernet_frame.png[]

=== Packeter

.Structural schematic of comcellnode bpmpacketer.
image::packeter_schematic.svg[]


.State machine
image::packeter_fsm.svg[]

=== Unpacketer

// ================================================================================
== Registers map
include::packeter_regmap.adoc[]
include::unpacketer_regmap.adoc[]

// ================================================================================
== TODO/Future of this module

* Output timestamps for latency computation ?.
