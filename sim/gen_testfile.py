import numpy as np


Nbpm=10
dty = np.dtype([("seqnum", "u2"), ("bpmid", "u2"), ("datax", "u4"), ("datay", "u4")])

bpms=np.arange(Nbpm)+13

def make_seq(start, nseq, bpms):
    Nbpm=len(bpms)
    data  = np.zeros(Nbpm*nseq, dtype=dty)
    data['seqnum']=np.arange(nseq, dtype="u2").repeat(Nbpm)+start
    data['bpmid'] = np.tile(bpms, nseq)
    data['datax']=np.random.randint(2**32-1, size=data.shape[0], dtype="u4")
    data['datay']=np.random.randint(2**32-1, size=data.shape[0], dtype="u4")
    return data

def make_zero(N):
    data  = np.zeros(N, dtype=dty)
    return data

sequences = make_zero(3)
cseq= np.empty(0, dtype=dty)
s=0
blanksize=120

#%% 13 sequences, each followed by a blank

for n in range(13):
    _s = make_seq(s,1, bpms)
    sequences = np.concatenate([sequences, _s , make_zero(blanksize),])
    if n!=0:
        cseq = np.concatenate([cseq, _s ])
    s+=1


#%% 13 sequences with missing BPM, each followed by a blank

for n in range(13):

    sequences = np.concatenate([sequences, make_seq(s,1, bpms[:-1]), make_zero(blanksize),])
    s+=1

#%% 13 sequences, each followed by a blank

for n in range(13):
    _s = make_seq(s,1, bpms)
    sequences = np.concatenate([sequences, _s , make_zero(blanksize),])
    if n!=0:
        cseq = np.concatenate([cseq, _s ])
    s+=1

#%% 13 sequences with one bpm not aligned, each followed by a blank

for n in range(13):
    ws=make_seq(s,1, bpms)
    ws[-4]['seqnum']=s+90
    sequences = np.concatenate([sequences, ws, make_zero(blanksize),])
    s+=1

#%% 13 sequences, each followed by a blank

for n in range(13):
    _s = make_seq(s,1, bpms)
    sequences = np.concatenate([sequences, _s , make_zero(blanksize),])
    if n!=0:
        cseq = np.concatenate([cseq, _s ])
    s+=1


# Remove first sequence, used for startup
cseq = cseq[cseq['seqnum']!=1]


#%%
# Print to file
with open("testinput.dat", 'w') as fp:
    for d in sequences:
        fp.write("{:04X} {:04X}{:08X}{:08X}\n".format(*d))

with open("testoutput.dat", 'w') as fp:
    for d in cseq:
        fp.write("{:02X} {:04X}{:08X}{:08X}\n".format(*d))

