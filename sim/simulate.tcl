exec rm -rf simproj
create_project sim_packeter simproj

set_property source_mgmt_mode All [current_project]

read_vhdl -vhdl2008 {sim/tb_ccn.vhd}
read_vhdl {hdl/ccn_unpack.vhd hdl/ccn_pack.vhd}
add_files -fileset sim_1 {sim/testinput.txt sim/testoutput.txt }

set CCN_DPKT_W 80
set CCN_UPKT_W 80
set CCN_DPKT_TU_W 16
set CCN_UPKT_TU_W 8
set CCN_FRAME_W 64
set CCN_FRAME_HEADER_W 192


source tcl/generate_axis.tcl

update_compile_order -fileset sim_1
launch_simulation
close_wave_config
open_wave_config sim/tb_ccn_global.wcfg
open_wave_config sim/tb_ccn_pack.wcfg
open_wave_config sim/tb_ccn_unpack.wcfg
restart
run 10 us

