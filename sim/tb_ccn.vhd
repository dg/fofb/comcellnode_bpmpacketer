library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.uniform;
use ieee.math_real.ceil;
use ieee.std_logic_textio.all;

use std.textio.all;

entity tb_ccn is
end entity;

architecture testbench of tb_ccn is

    --------------------------
    -- CONSTANT DECLARATION --
    --------------------------
    constant RX_COOLDOWN         :  natural := 0;
    constant PERIOD              :  time := 4 ns;
    constant SCALED_PPS_PERIOD   :  natural := 250; -- PPS = 1µs 

    constant PROTOCOL_ID         :  std_logic_vector(7 downto 0) := x"75";
    constant IPKT_TDATA_W        :  natural := 80;
    constant IPKT_TUSER_W        :  natural := 16;
    constant OPKT_TDATA_W        :  natural := 80;
    constant OPKT_TUSER_W        :  natural := 8;
    constant FRAME_TDATA_W       :  natural := 64;
    constant FRAME_TUSER_W       :  natural := 1;

    ------------------------
    -- SIGNAL DECLARATION --
    ------------------------
    signal tb_clk                :  std_logic                       := '0';
    signal tb_m_clk              :  std_logic                       := '0';
    signal tb_rstn               :  std_logic                       := '1';
    signal tb_timeref            :  unsigned(63 downto 0);

    signal tb_pps                :  std_logic                       := '0';

    -- Input packet axis
    signal tb_s_axis_tuser       :  std_logic_vector(IPKT_TUSER_W-1 downto 0);
    signal tb_s_axis_tdata       :  std_logic_vector(IPKT_TDATA_W-1 downto 0);
    signal tb_s_axis_tvalid      :  std_logic;
    signal tb_s_axis_tready      :  std_logic;

    -- Output packet axis
    signal tb_m_axis_tvalid      :  std_logic;
    signal tb_m_axis_tready      :  std_logic;
    signal tb_m_axis_tdata       :  std_logic_vector(OPKT_TDATA_W-1 downto 0);
    signal tb_m_axis_tkeep       :  std_logic_vector(OPKT_TDATA_W/8-1 downto 0);
    signal tb_m_axis_tlast       :  std_logic;
    signal tb_m_axis_tuser       :  std_logic_vector(OPKT_TUSER_W-1 downto 0);

    -- In between frame axis
    signal eth_axis_tvalid       :  std_logic;
    signal eth_axis_tready       :  std_logic;
    signal eth_axis_tdata        :  std_logic_vector(FRAME_TDATA_W-1 downto 0);
    signal eth_axis_tkeep        :  std_logic_vector(FRAME_TDATA_W/8-1 downto 0);
    signal eth_axis_tlast        :  std_logic;
    signal eth_axis_tuser        :  std_logic_vector(FRAME_TUSER_W-1 downto 0);

    -- Packeter control
    signal tb_pack_run           :  std_logic;
    signal tb_pack_mac_dst       :  std_logic_vector(47 downto 0);
    signal tb_pack_mac_src       :  std_logic_vector(47 downto 0);
    signal tb_pack_mac_length    :  std_logic_vector(15 downto 0);
    signal tb_pack_expect_pkt    :  std_logic_vector(7 downto 0);
    signal tb_pack_timeout       :  std_logic_vector(15 downto 0);
    signal tb_pkt_rate_cnt       :  std_logic_vector(23 downto 0);


    -- Packeter status
    signal tb_pack_err_seq       :  std_logic;
    signal tb_pack_err_timeout   :  std_logic;
    signal tb_pack_frame_count   :  std_logic_vector(31 downto 0);

    -- Unpacketer control
    signal tb_unpack_enable      :  std_logic;
    signal tb_unpack_mac_dst     :  std_logic_vector(47 downto 0);
    signal tb_unpack_mac_src     :  std_logic_vector(47 downto 0);
    signal tb_unpack_mac_length  :  std_logic_vector(15 downto 0);

    -- Unpacketer status
    signal tb_unpack_err_pid     :  std_logic;
    signal tb_unpack_err_mac_len :  std_logic;
    signal tb_unpack_err_mac_dst :  std_logic;
    signal tb_unpack_err_mac_src :  std_logic;
    signal tb_unpack_mcts        :  std_logic_vector(63 downto 0);

    signal tx_send               :  boolean                         := false;
    signal rx_recv               :  boolean                         := false;
    
    signal tb_pps_vec            : std_logic_vector(8 downto 0);
    signal tb_pps_cnt            : unsigned(8 downto 0);
    

begin

    -- clock generation
    tb_clk      <= not tb_clk   after PERIOD;
    tb_m_clk    <= not tb_m_clk after 6.4 ns;

    --------------------------
    -- DUT PACKER INSTANCIATION --
    --------------------------
    inst_dut_pack: entity work.ccn_pack
    generic map(
        G_S_TDATA_W          => IPKT_TDATA_W,
        G_S_TUSER_W          => IPKT_TUSER_W,
        G_M_TDATA_W          => FRAME_TDATA_W,
        G_M_TUSER_W          => FRAME_TUSER_W,
        G_PROTOCOL_ID        => PROTOCOL_ID
    )
    port map(
        aclk                 => tb_clk,
        aresetn              => tb_rstn,
	    pps		             => tb_pps,

        -- Control
        packeter_run         => tb_pack_run,
        mac_dst              => tb_pack_mac_dst,
        mac_src              => tb_pack_mac_src,
        mac_length           => tb_pack_mac_length,
        packet_timeout       => tb_pack_timeout,
        expect_pkt_cnt       => tb_pack_expect_pkt,
        timeref              => std_logic_vector(tb_timeref),
        latch_seq            => '0',
        pkt_rate_cnt         => tb_pkt_rate_cnt,

        -- AXIS Packet input
        s_axis_tdata         => tb_s_axis_tdata,
        s_axis_tuser         => tb_s_axis_tuser,
        s_axis_tvalid        => tb_s_axis_tvalid,
        s_axis_tready        => tb_s_axis_tready,

        -- AXIS Frame output
        m_axis_clk           => tb_m_clk,
        m_axis_tvalid        => eth_axis_tvalid,
        m_axis_tready        => eth_axis_tready,
        m_axis_tdata         => eth_axis_tdata,
        m_axis_tkeep         => eth_axis_tkeep,
        m_axis_tlast         => eth_axis_tlast,
        m_axis_tuser         => eth_axis_tuser,

        latched_seq1         => open,
        latched_seq2         => open,
        status_err_seq       => tb_pack_err_seq,
        status_err_timeout   => tb_pack_err_timeout,
        status_frame_count   => tb_pack_frame_count

    );

    --------------------------
    -- DUT RX INSTANCIATION --
    --------------------------
    inst_dut_unpack: entity work.ccn_unpack
    generic map(
        G_S_TDATA_W          => FRAME_TDATA_W,
        G_S_TUSER_W          => FRAME_TUSER_W,
        G_M_TDATA_W          => OPKT_TDATA_W,
        G_M_TUSER_W          => OPKT_TUSER_W,
        G_PROTOCOL_ID        => PROTOCOL_ID
    )
    port map(
        rstn    => tb_rstn,
        pps     => '0',

        -- Control
        enable           => tb_unpack_enable,
        mac_len          => tb_unpack_mac_length,
        mac_dst          => tb_unpack_mac_dst,
        mac_src          => tb_unpack_mac_src,

        -- Status
        err_pid         => tb_unpack_err_pid,
        err_mac_len     => tb_unpack_err_mac_len,
        err_mac_dst     => tb_unpack_err_mac_dst,
        err_mac_src     => tb_unpack_err_mac_src,
        pkt_mcts        => tb_unpack_mcts,
        frame_counter   => open,

        -- AXIS Frame input
        s_axis_clk      => tb_m_clk,
        s_axis_tvalid    => eth_axis_tvalid,
        s_axis_tready    => eth_axis_tready,
        s_axis_tdata     => eth_axis_tdata,
        s_axis_tkeep     => eth_axis_tkeep,
        s_axis_tlast     => eth_axis_tlast,
        s_axis_tuser     => eth_axis_tuser,

        -- AXIS Packet output
        m_axis_clk      => tb_clk,
        m_axis_tvalid    => tb_m_axis_tvalid,
        m_axis_tready    => tb_m_axis_tready,
        m_axis_tdata     => tb_m_axis_tdata,
        m_axis_tkeep     => tb_m_axis_tkeep,
        m_axis_tuser     => tb_m_axis_tuser,
        m_axis_tlast     => tb_m_axis_tlast

    );


    ------------------
    -- MAIN PROCESS --
    ------------------
    p_main: process
        variable randint : positive;
        variable x : real;
        variable seed1 : positive := 1;
        variable seed2 : positive := 2;
    begin

        -- let it running then reset, synchronous deassertion
        wait for PERIOD*7+7 ns;
        tb_rstn <= '0';
        wait until rising_edge(tb_clk);
        tb_pack_run          <= '0';
        tb_pack_timeout      <= (others => '0');
        tb_pack_mac_dst      <= (others => '0');
        tb_pack_mac_src      <= (others => '0');
        tb_pack_mac_length   <= (others => '0');
        tb_pack_expect_pkt   <= (others => '0');
        tb_unpack_mac_dst    <= (others => '0');
        tb_unpack_mac_src    <= (others => '0');
        tb_unpack_mac_length <= (others => '0');
        tb_unpack_enable     <= '0';

        wait for 4*PERIOD;

        wait until rising_edge(tb_clk);
        tb_rstn <= '1';
        wait until rising_edge(tb_clk);



        for I in 0 to 20 loop
            wait until rising_edge(tb_clk);
        end loop;




        -- Configuration values
        tb_pack_run         <= '1';
        tb_unpack_enable    <= '1';
        tb_pack_timeout     <= x"0040";
        tb_pack_mac_dst     <= x"010000DBAAFF";
        tb_pack_mac_src     <= x"050000DBAAFF";
        tb_unpack_mac_dst   <= x"010000DBAAFF";
        tb_unpack_mac_src   <= x"050000DBAAFF";
        tb_pack_expect_pkt  <= std_logic_vector(to_unsigned(10-1, tb_pack_expect_pkt'length));
        tb_pack_mac_length  <= std_logic_vector(to_unsigned(10*10+10, tb_pack_mac_length'length));
        tb_unpack_mac_length<= std_logic_vector(to_unsigned(10*10+10, tb_pack_mac_length'length));

        wait until rising_edge(tb_clk);

        -- Enable RX after a time
        --rx_recv <= true after 50*PERIOD;

        -- Send some BPM frames
        tx_send <= true;
        rx_recv <= true;


        -- Never ending end
        wait;

    end process p_main;


    ---------------
    -- TX STREAM --
    ---------------
    p_tx_send:process

        file testinput : TEXT open READ_MODE is "testinput.dat";
        variable linenum : natural :=0;
        variable text_line : line;
        variable readok : boolean;

        variable vr_tuser : std_logic_vector(IPKT_TUSER_W-1 downto 0);
        variable vr_tdata : std_logic_vector(IPKT_TDATA_W-1 downto 0);

    begin
        tb_s_axis_tvalid   <= '0';

        while true loop
            if not tx_send then
                wait until tx_send;
                wait until rising_edge(tb_clk);
            end if;

            if endfile(testinput) then
                report "End of file reached." severity warning;
                exit;
            end if;

            readline(testinput, text_line);
            linenum := linenum+1;

            hread(text_line, vr_tuser, readok);
            assert readok
                report "Read 'tuser' failed for line: " & integer'image(linenum) severity failure;

            hread(text_line, vr_tdata, readok);
            assert readok
                report "Read 'tdata' failed for line: " & integer'image(linenum) severity failure;


            tb_s_axis_tdata <= vr_tdata;
            tb_s_axis_tuser <= vr_tuser;
            -- Special on data=0, just let it roll as if no data ready
            tb_s_axis_tvalid   <= '1' when unsigned(vr_tdata) /= 0 else '0';

            wait until rising_edge(tb_clk) and tb_s_axis_tready='1';
            tb_s_axis_tvalid   <= '0';

        end loop;

        wait;

    end process p_tx_send;

    ---------------
    -- RX STREAM --
    ---------------
    p_rx_recv:process

        file testoutput : TEXT open READ_MODE is "testoutput.dat";
        variable linenum : natural :=0;
        variable text_line : line;
        variable readok : boolean;

        variable vr_tuser : std_logic_vector(OPKT_TUSER_W-1 downto 0);
        variable vr_tdata : std_logic_vector(OPKT_TDATA_W-1 downto 0);


    begin
        while true loop
            if not rx_recv then
                tb_m_axis_tready   <= '0';
                wait until rx_recv and rising_edge(tb_clk);
            end if;

            tb_m_axis_tready   <= '1';
            wait until rising_edge(tb_clk) and tb_m_axis_tvalid ='1';
            tb_m_axis_tready   <= '0';

            if endfile(testoutput) then
                report "End of file reached." severity warning;
                exit;
            end if;

            readline(testoutput, text_line);
            linenum := linenum+1;

            hread(text_line, vr_tuser, readok);
            assert readok
                report "Read 'tuser' failed for line: " & integer'image(linenum) severity failure;

            hread(text_line, vr_tdata, readok);
            assert readok
                report "Read 'tdata' failed for line: " & integer'image(linenum) severity failure;

            assert tb_m_axis_tuser = vr_tuser
                report "Packet output tuser check mismatch. Got " &
                to_hex_string(tb_m_axis_tuser) & " ; Expected " & to_hex_string(vr_tuser)
                severity warning;

            assert tb_m_axis_tdata = vr_tdata
                report "Packet output tdata check mismatch. Got " &
                to_hex_string(tb_m_axis_tdata) & " ; Expected " & to_hex_string(vr_tdata)
                severity warning;


            for I in 1 to RX_COOLDOWN loop
                wait until rising_edge(tb_clk);
            end loop;
        end loop;
    end process p_rx_recv;

    ---------------------
    -- TIMEREF COUNTER --
    ---------------------
    p_timeref: process(tb_clk, tb_rstn)
    begin
        if tb_rstn = '0' then
            tb_timeref <= (others => '0');
        elsif rising_edge(tb_clk) then
            tb_timeref <= tb_timeref+1;
        end if;
    end process p_timeref;
 
 
     ---------------------
    -- PPS GEN --
    --------------------- 
    tb_pps <= tb_pps_vec(tb_pps_vec'high) when tb_pps_vec = "100000000" else '0';
    
    
    p_ppsgen: process(tb_clk, tb_rstn)
    begin
        if tb_rstn = '0' then
            tb_pps_cnt <= (others => '0');
        elsif rising_edge(tb_clk) then
            if tb_pps_cnt = 256 then
                tb_pps_vec <= std_logic_vector(resize(unsigned(tb_pps_cnt),tb_pps_vec'length));
                tb_pps_cnt <= (others => '0');  
            else
                tb_pps_cnt <= tb_pps_cnt+1;
                tb_pps_vec <= std_logic_vector(resize(unsigned(tb_pps_cnt),tb_pps_vec'length));
            end if;
        end if;
    end process p_ppsgen; 


end architecture testbench;

