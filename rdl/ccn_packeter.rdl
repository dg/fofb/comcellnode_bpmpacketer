`include "ccn_packeter.vh"

`define C_ID 0x507E1743

addrmap ccn_packeter {

    desyrdl_generate_hdl = true;
    desyrdl_interface = "AXI4L";

    reg {
      desc="Module Identification Number";
      default sw = r;
      default hw = r;
      field {} data[32] = `C_ID;
    } ID @0x00;

    reg {
        desc="Module version.";
        field {hw=w;sw=r;} data[32];
    } VERSION @0x04;

    reg {
        desc="Ethernet MAC frame destination address. Least Significant Bytes.";
        field {hw=r; sw=rw;} data[32];
    } MAC_DST_LSB;

    reg {
        desc="Ethernet MAC frame destination address. Most Significant Bytes.";
        field {hw=r; sw=rw;} data[16];
    } MAC_DST_MSB;

    reg {
        desc="Ethernet MAC frame source address. Least Significant Bytes.";
        field {hw=r; sw=rw;} data[32];
    } MAC_SRC_LSB;

    reg {
        desc="Ethernet MAC frame source address. Most Significant Bytes.";
        field {hw=r; sw=rw;} data[16];
    } MAC_SRC_MSB;

    reg {
        desc="Ethernet MAC frame length. Should be (NPACKET+1)*PACKETSIZE+10";
        field {hw=r; sw=rw;} data[16];
    } MAC_LENGTH;

    reg {
        desc="Packeter control.";
        default hw=r; default sw=rw;
        field {desc="Enable the BPM packeter.";} ENABLE;
        field {desc="Latch sequence registers";} LATCHSEQ;
    } CONTROL;

    reg {
        default hw=r; default sw=rw;
        field {desc="Timeout limit. One unit is 4 ns.";} data[16];
    } TIMEOUT;

    reg {
        default hw=r; default sw=rw;
        field {desc="Number of BPM packet to aggregate -1 .";} data[8];
    } NPACKET;

    reg {
        desc="Number of output Position Packets counted";
        field {hw=w; sw=r;} data[32];
    } PACKETER_COUNT;

    reg {
        desc="Number of Position Packets counted per second";
        field {hw=w; sw=r;} pps_pck_cnt[32];
    } PPS_PACKETER_RATE;

    reg {
        desc="Error detected by the packeter. Cleared on read.";
        field {hw=w; sw=r; desc="New FA sequence received before reaching expected number of BPM packets.";
        } SEQ;
        field {hw=w; sw=r; desc="Timeout waiting for a new BPM packet.";
        } TIMEOUT;
    } PACKETER_ERROR;

    reg {
        desc="Reset error flags";
        default hw=r; default sw=rw;
        field {desc="Start the BPM packeter.";} RESET;
    } RESET_ERROR;

    reg {
        desc="Latched sequence register 1";
        field {hw=w; sw=r;} data[16];
    } LATCHED_SEQ1;

    reg {
        desc="Latched sequence register 2";
        field {hw=w; sw=r;} data[16];
    } LATCHED_SEQ2;

};
