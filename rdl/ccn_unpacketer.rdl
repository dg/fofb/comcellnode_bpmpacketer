`include "ccn_unpacketer.vh"

`define C_ID 0x507E1746

addrmap ccn_unpacketer {

    desyrdl_generate_hdl = true;
    desyrdl_interface = "AXI4L";

    reg {
      desc="Module Identification Number";
      default sw = r;
      default hw = r;
      field {} data[32] = `C_ID;
    } ID @0x00;

    reg {
        desc="Module version.";
        field {hw=w;sw=r;} data[32];
    } VERSION @0x04;

    reg {
        desc="Ethernet MAC frame destination address. Least Significant Bytes.";
        field {hw=r; sw=rw;} data[32];
    } MAC_DST_LSB;

    reg {
        desc="Ethernet MAC frame destination address. Most Significant Bytes.";
        field {hw=r; sw=rw;} data[16];
    } MAC_DST_MSB;

    reg {
        desc="Ethernet MAC frame source address. Least Significant Bytes.";
        field {hw=r; sw=rw;} data[32];
    } MAC_SRC_LSB;

    reg {
        desc="Ethernet MAC frame source address. Most Significant Bytes.";
        field {hw=r; sw=rw;} data[16];
    } MAC_SRC_MSB;

    reg {
        desc="Ethernet MAC frame length. Should be (CONFIGURE.NPACKET+1)*PACKETSIZE+10";
        field {hw=r; sw=rw;} LENGTH[16];
    } MAC_LENGTH;

    reg {
        desc="Unpacketer control.";
        default hw=r; default sw=rw;
        field {desc="Start the BPM packeter.";} ENABLE;
    } CONTROL;

    reg {
        desc="Error detected by the packeter. Cleared on read.";
        field {hw=w; sw=r; desc="Protocol ID mismatch.";
        } ERR_PID;
        field {hw=w; sw=r; desc="Incomming MAC frame length not expected.";
        } ERR_MAC_LEN;
        field {hw=w; sw=r; desc="Incomming MAC source address not expected.";
        } ERR_MAC_SRC;
        field {hw=w; sw=r; desc="Incomming MAC destination address not expected.";
        } ERR_MAC_DST;
    } UNPACK_ERROR;

    reg {
        desc="Reset error flags";
        default hw=r; default sw=rw;
        field {desc="Start the BPM packeter.";} RESET;
    } RESET_ERROR;

    reg {
        desc="Unpacked frames counter.";
        field {hw=rw; sw=r;} data[32];
    } FRAME_CNT;

};
