library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.or_reduce;

entity ccn_pack is
    generic(
        G_S_TDATA_W          : positive;
        G_S_TUSER_W          : positive;
        G_M_TDATA_W          : positive;
        G_M_TUSER_W          : positive;
        G_PROTOCOL_ID        : std_logic_vector(7 downto 0)
    );
    port(
        aclk                 : in std_logic;
        aresetn              : in std_logic;
        pps                  : in std_logic;

        -- Control
        packeter_run         : in std_logic;
        mac_dst              : in std_logic_vector(47 downto 0);
        mac_src              : in std_logic_vector(47 downto 0);
        mac_length           : in std_logic_vector(15 downto 0);
        packet_timeout       : in std_logic_vector(15 downto 0);
        expect_pkt_cnt       : in std_logic_vector(7 downto 0);
        timeref              : in std_logic_vector(63 downto 0);
        latch_seq            : in std_logic;
        pkt_rate_cnt	     : out std_logic_vector(23 downto 0); -- >200 BPM @10KHz 
	

        -- Status
        status_err_seq       : out std_logic;
        status_err_timeout   : out std_logic;
        status_frame_count   : out std_logic_vector(31 downto 0);
        latched_seq1         : out std_logic_vector(15 downto 0);
        latched_seq2         : out std_logic_vector(15 downto 0);

        -- AXIS Packet input
        s_axis_tdata         : in std_logic_vector(G_S_TDATA_W-1 downto 0);
        s_axis_tuser         : in std_logic_vector(G_S_TUSER_W-1 downto 0);
        s_axis_tvalid        : in std_logic;
        s_axis_tready        : out std_logic;

        -- AXIS Frame output
        m_axis_clk           : in std_logic; -- Only M_AXIS signals on this clk domain
        m_axis_tdata         : out std_logic_vector(G_M_TDATA_W-1 downto 0); 
        m_axis_tkeep         : out std_logic_vector(G_M_TDATA_W/8-1 downto 0);
        m_axis_tlast         : out std_logic;
        m_axis_tvalid        : out std_logic;
        m_axis_tready        : in std_logic;
        m_axis_tuser         : out std_logic_vector(G_M_TUSER_W-1 downto 0)

        );
end entity ccn_pack;


architecture rtl of ccn_pack is

    ----------------------
    -- TYPE DECLARATION --
    ----------------------
    type t_state is (STANDBY, FIRST, RUN, ERR_TO, ERR_SEQ);

    ------------------------
    -- SIGNAL DECLARATION --
    ------------------------
    -- AXIS REG
    signal pkt_reg_tvalid           : std_logic;
    signal pkt_reg_tready           : std_logic;
    signal pkt_reg_tdata            : std_logic_vector(G_S_TDATA_W-1 downto 0);
    signal pkt_reg_tuser            : std_logic_vector(G_S_TUSER_W-1 downto 0);

    -- Sequence detector
    signal prev_seq_r               : std_logic_vector(G_S_TUSER_W-1 downto 0);
    signal new_seq_valid_r          : std_logic;
    signal new_seq                  : std_logic;

    -- Dump/pass
    signal reg_tvalid               : std_logic;

    -- Packet counter
    signal pkt_cnt                  : unsigned(7 downto 0);
    signal pkt_cnt_decr             : std_logic;
    signal pkt_cnt_zero             : std_logic;
    signal load_pkt_cnt_ena         : std_logic;

    -- Packet Rate Counter 
    signal pps_detect               : std_logic_vector(1 downto 0);
    signal is_pps                   : std_logic;
    signal pkt_rt_cnt               : unsigned(23 downto 0);
    signal pkt_rt_cnt_v             : std_logic_vector(23 downto 0); 
    signal pkt_rt_cnt_ena           : std_logic;
    signal pkt_cnt_incr             : std_logic;

    -- Frame counter
    signal frame_counter            : unsigned(31 downto 0);

    -- Interconnex, header inserter
    signal pkt_ix_tvalid            : std_logic;
    signal pkt_ix_tready            : std_logic;
    signal pkt_ix_tlast             : std_logic;
    signal pkt_ix_tuser             : std_logic_vector(G_S_TDATA_W/8-1 downto 0);
    signal pkt_ix_tdata             : std_logic_vector(G_S_TDATA_W-1 downto 0);
    signal header_tvalid            : std_logic;
    signal header_tready            : std_logic;
    signal header_tdata             : std_logic_vector(191 downto 0);
    signal sup_arb_res_s1           : std_logic;

    -- FSM
    signal fsm_state                : t_state;
    signal fsm_state_next           : t_state;
    signal frame_timeout            : std_logic;
    signal dump                     : std_logic;
    signal pass                     : std_logic;
    signal frame_error              : std_logic;
    signal m_tvalid_r               : std_logic;

    -- Timeout
    signal timeout_cnt              : unsigned(15 downto 0);

    -- Output FIFO
    signal fifo_tvalid              : std_logic;
    signal fifo_tvalid_c            : std_logic;
    signal fifo_tready              : std_logic;
    signal fifo_tready_c            : std_logic;
    signal fifo_tdata               : std_logic_vector(G_M_TDATA_W-1 downto 0);
    signal fifo_tkeep               : std_logic_vector(7 downto 0);
    signal fifo_tlast               : std_logic;
    signal fifo_tuser               : std_logic_vector(G_M_TUSER_W-1 downto 0);
    signal fifo_tuser_slv           : std_logic_vector(7 downto 0);
    signal fifo_err_rst             : std_logic;
    signal fifo_clr                 : std_logic;

    -- Some constant for mapping
    constant C_TKEEP_S00            : std_logic_vector(23 downto 0)              := (others => '1');
    constant C_TUSER_S00            : std_logic_vector(23 downto 0)              := (others => '0');
    constant C_TKEEP_S01            : std_logic_vector(G_S_TDATA_W/8-1 downto 0) := (others => '1');
    constant C_LOGIC_ZERO           : std_logic                                  := '0';

begin

    ---------------------------
    -- PACKET INPUT REGISTER --
    ---------------------------
    inst_regslice : entity work.ccn_axis_reg_packet
    port map (
        aclk          => aclk,
        aresetn       => aresetn,
        s_axis_tvalid => s_axis_tvalid,
        s_axis_tready => s_axis_tready,
        s_axis_tdata  => s_axis_tdata,
        s_axis_tuser  => s_axis_tuser,
        m_axis_tvalid => pkt_reg_tvalid,
        m_axis_tready => pkt_reg_tready,
        m_axis_tuser  => pkt_reg_tuser,
        m_axis_tdata  => pkt_reg_tdata
    );

    ---------------------------
    -- NEW SEQUENCE DETECTOR --
    ---------------------------
    new_seq         <= (new_seq_valid_r and pkt_reg_tvalid) when pkt_reg_tuser /= prev_seq_r else '0';

    p_seq_detect:process(aclk, aresetn)
    begin
        if aresetn = '0' then
            prev_seq_r          <= (others => '0');
            new_seq_valid_r     <= '0';
            m_tvalid_r          <= '0';
            latched_seq1        <= (others => '0');
            latched_seq2        <= (others => '0');
        elsif rising_edge(aclk) then
            if (pkt_reg_tvalid and pkt_reg_tready) = '1' then
                prev_seq_r      <= pkt_reg_tuser;
                new_seq_valid_r <= '1';

                if latch_seq = '0' and (pkt_reg_tuser /= prev_seq_r) then
                    latched_seq1    <= std_logic_vector(resize(unsigned(pkt_reg_tuser), 16));
                    latched_seq2    <= std_logic_vector(resize(unsigned(prev_seq_r), 16));
                end if;

            end if;
            m_tvalid_r          <= pkt_reg_tvalid;
        end if;
    end process;

    ----------------------------
    -- DUMP/PASS AXIS CIRCUIT --
    ----------------------------
    pkt_reg_tready      <= (dump and m_tvalid_r) or (pass and pkt_ix_tready);
    reg_tvalid          <= pass and pkt_reg_tvalid;


    --------------------
    -- PACKET COUNTER --
    --------------------
    pkt_cnt_decr      <= reg_tvalid and pkt_reg_tready;
    pkt_cnt_zero      <= '1' when pkt_cnt = 0 else '0';

    p_pkt_cnt:process(aclk, aresetn)
    begin
        if aresetn = '0' then
            pkt_cnt   <= (others => '0');

        elsif rising_edge(aclk) then
            if load_pkt_cnt_ena = '1' then
                pkt_cnt <= unsigned(expect_pkt_cnt);
            else
                if pkt_cnt_decr = '1' then
                    pkt_cnt <= pkt_cnt -1;
                end if;
            end if;
        end if;
    end process p_pkt_cnt;

    -------------------------
    -- PPS PULSE DETECTION --
    -------------------------
    is_pps <= pps_detect(0) and not(pps_detect(1));
    
    p_pps_dtc:process(aclk, aresetn)
    begin
        if aresetn = '0' then
            pps_detect <= (others => '0');

        elsif rising_edge(aclk) then
		pps_detect(1) <= pps_detect(0);
		pps_detect(0) <= pps;
        end if;
    end process p_pps_dtc;

    -------------------------
    -- PACKET RATE COUNTER --
    -------------------------
    pkt_cnt_incr      <= reg_tvalid and pkt_reg_tready;  -- same conditions as for pkt decounter

    pkt_rate_cnt <= pkt_rt_cnt_v;

    p_pkt_rt_cnt:process(aclk, aresetn) -- counter process 
    begin
        if aresetn = '0' then
            pkt_rt_cnt   <= (others => '0');

        elsif rising_edge(aclk) then
            if is_pps = '1' then       
                pkt_rt_cnt_v <= std_logic_vector(pkt_rt_cnt);
                pkt_rt_cnt <= (others => '0');               -- reset packet rate value 
            else
                if pkt_rt_cnt_ena = '1' and pkt_cnt_incr = '1' then
                    pkt_rt_cnt <= pkt_rt_cnt+1;              --  incr pkt rate cnt 
                end if;
            end if;
        end if;
    end process p_pkt_rt_cnt;


    -------------
    -- TIMEOUT --
    -------------
    p_timeout:process(aclk, aresetn)
    begin
        if aresetn = '0' then
            timeout_cnt <= (others => '0');
        elsif rising_edge(aclk) then
            if fsm_state = RUN then
                timeout_cnt <= timeout_cnt - 1;
            else
                timeout_cnt <= unsigned(packet_timeout);
            end if;
        end if;
    end process p_timeout;

    frame_timeout <= '1' when timeout_cnt = 0 else '0';

    --------------------
    -- FRAME COUNTER --
    --------------------
    p_frame_cnt:process(aclk, aresetn)
    begin
        if aresetn = '0' then
            frame_counter  <= (others => '0');
        elsif rising_edge(aclk) then
            if packeter_run = '0' then
                frame_counter <= (others => '0');
            else
                if (pkt_ix_tvalid and pkt_ix_tready and pkt_ix_tlast and not pkt_ix_tuser(0)) =  '1' then
                    frame_counter  <= frame_counter+1;
                end if;
            end if;
        end if;
    end process p_frame_cnt;

    ----------------
    -- FIFO CLEAR --
    ----------------
    p_fifo_clr:process(aclk, aresetn)
    begin
        if aresetn = '0' then
            fifo_clr  <= '0';
        elsif rising_edge(aclk) then
            if fifo_tuser(0) = '1' then
                fifo_clr        <= '1';
            else
                if fifo_tlast = '1' then
                    fifo_clr    <= '0';
                end if;
            end if;
        end if;
    end process p_fifo_clr;

    ----------------------
    -- HEADER INSERTION --
    ----------------------
    header_tvalid   <= '1' when fsm_state = FIRST and pkt_ix_tready = '1' else '0';

    header_tdata(7 downto 0)     <= mac_dst(47 downto 40);
    header_tdata(15 downto 8)    <= mac_dst(39 downto 32);
    header_tdata(23 downto 16)   <= mac_dst(31 downto 24);
    header_tdata(31 downto 24)   <= mac_dst(23 downto 16);
    header_tdata(39 downto 32)   <= mac_dst(15 downto 8);
    header_tdata(47 downto 40)   <= mac_dst(7 downto 0);
    header_tdata(55 downto 48)   <= mac_src(47 downto 40);
    header_tdata(63 downto 56)   <= mac_src(39 downto 32);
    header_tdata(71 downto 64)   <= mac_src(31 downto 24);
    header_tdata(79 downto 72)   <= mac_src(23 downto 16);
    header_tdata(87 downto 80)   <= mac_src(15 downto 8);
    header_tdata(95 downto 88)   <= mac_src(7 downto 0);
    header_tdata(103 downto 96)  <= mac_length(15 downto 8);
    header_tdata(111 downto 104) <= mac_length(7 downto 0);
    header_tdata(119 downto 112) <= G_PROTOCOL_ID;
    header_tdata(127 downto 120) <= pkt_reg_tuser(7 downto 0);
    header_tdata(191 downto 128) <= timeref;

    sup_arb_res_s1 <= not header_tready;

    pkt_ix_tdata       <= pkt_reg_tdata when frame_error = '0' else
                          (others => '0');
    pkt_ix_tvalid      <= frame_error or reg_tvalid;
    pkt_ix_tlast       <= frame_error or pkt_cnt_zero;
    pkt_ix_tuser       <= (others => frame_error);

    inst_interco: entity work.ccn_axis_header_interco
    port map (
        aclk                 => aclk,
        aresetn              => aresetn,
        s00_axis_aclk        => aclk,
        s01_axis_aclk        => aclk,
        m00_axis_aclk        => aclk,
        s00_axis_aresetn     => aresetn,
        s01_axis_aresetn     => aresetn,
        m00_axis_aresetn     => aresetn,
        s00_axis_tvalid      => header_tvalid,
        s00_axis_tready      => header_tready,
        s00_axis_tdata       => header_tdata,
        s00_axis_tkeep       => C_TKEEP_S00,
        s00_axis_tlast       => C_LOGIC_ZERO,
        s00_axis_tuser       => C_TUSER_S00,
        s01_axis_tvalid      => pkt_ix_tvalid,
        s01_axis_tready      => pkt_ix_tready,
        s01_axis_tdata       => pkt_ix_tdata,
        s01_axis_tkeep       => C_TKEEP_S01,
        s01_axis_tlast       => pkt_ix_tlast,
        s01_axis_tuser       => pkt_ix_tuser,
        m00_axis_tvalid      => fifo_tvalid,
        m00_axis_tready      => fifo_tready_c,
        m00_axis_tdata       => fifo_tdata,
        m00_axis_tkeep       => fifo_tkeep,
        m00_axis_tlast       => fifo_tlast,
        m00_axis_tuser       => fifo_tuser_slv,
        s00_arb_req_suppress => C_LOGIC_ZERO,
        s01_arb_req_suppress => sup_arb_res_s1
    );

    ----------
    -- FIFO --
    ----------
    fifo_tuser(0) <= or_reduce(fifo_tuser_slv);
    fifo_err_rst  <= (not fifo_clr) and aresetn;
    fifo_tready_c <= fifo_tready or fifo_clr;
    fifo_tvalid_c <= fifo_tvalid and not fifo_clr;

    inst_fifo: entity work.ccn_axis_fifo_pframe
    port map(
        s_axis_aresetn => fifo_err_rst,
        s_axis_aclk    => aclk,
        m_axis_aclk    => m_axis_clk,
        s_axis_tvalid  => fifo_tvalid_c,
        s_axis_tready  => fifo_tready,
        s_axis_tdata   => fifo_tdata,
        s_axis_tkeep   => fifo_tkeep,
        s_axis_tlast   => fifo_tlast,
        s_axis_tuser   => fifo_tuser,
        m_axis_tvalid  => m_axis_tvalid,
        m_axis_tready  => m_axis_tready,
        m_axis_tdata   => m_axis_tdata,
        m_axis_tkeep   => m_axis_tkeep,
        m_axis_tlast   => m_axis_tlast,
        m_axis_tuser   => m_axis_tuser
    );


    ---------
    -- FSM --
    ---------
    p_fsm_sync:process(aclk, aresetn)
    begin
        if aresetn = '0' then
            fsm_state   <= STANDBY;
        elsif rising_edge(aclk) then
            fsm_state   <= fsm_state_next;
        end if;
    end process p_fsm_sync;

    p_fsm_comb:process(fsm_state, packeter_run, new_seq, pkt_cnt_zero, pkt_ix_tready, pkt_ix_tvalid, frame_timeout)
    begin
        case fsm_state is
            when STANDBY =>
                dump                    <= '1';
                load_pkt_cnt_ena        <= '1';
                pass                    <= '0';
                frame_error             <= '0';
                pkt_rt_cnt_ena          <= '0'; -- pkt_rt counter disable 
                --
                if (packeter_run and new_seq) = '1' then
                    fsm_state_next <= FIRST;
                else
                    fsm_state_next <= STANDBY;
                end if;

            when FIRST =>
                dump                    <= '0';
                load_pkt_cnt_ena        <= '0';
                pass                    <= '1';
                frame_error             <= '0';
                pkt_rt_cnt_ena          <= '1';-- pkt_rt counter enable 
                --
                if pkt_ix_tready = '1' then
                    fsm_state_next <= RUN;
                else
                    fsm_state_next <= FIRST;
                end if;

            when RUN =>
                dump                    <= '0';
                load_pkt_cnt_ena        <= '0';
                pass                    <= '1';
                frame_error             <= '0';
		        pkt_rt_cnt_ena          <= '1';-- pkt_rt counter enable 
                --
                if new_seq  = '1' then
                    fsm_state_next <= ERR_SEQ;
                elsif frame_timeout = '1' then
                    fsm_state_next <= ERR_TO;
                elsif pkt_cnt_zero = '1' and pkt_ix_tready = '1' and pkt_ix_tvalid = '1' then
                    fsm_state_next <= STANDBY;
                else
                    fsm_state_next <= RUN;
                end if;

            when ERR_TO =>
                dump                    <= '1';
                load_pkt_cnt_ena        <= '0';
                pass                    <= '0';
                frame_error             <= '1';
                pkt_rt_cnt_ena          <= '1';-- pkt_rt counter enable even if not necessayr - synthesis optimization ;) 
                --
                fsm_state_next <= STANDBY;

            when ERR_SEQ =>
                dump                    <= '1';
                load_pkt_cnt_ena        <= '0';
                pass                    <= '0';
                frame_error             <= '1';
                pkt_rt_cnt_ena          <= '1';-- pkt_rt counter enable : pps rate cnt --> even if a packet does not belong to the current frame, it is a packet. 
                --
                fsm_state_next <= STANDBY;

            when others =>
                dump                    <= '1';
                load_pkt_cnt_ena        <= '1';
                pass                    <= '0';
                frame_error             <= '0';
                pkt_rt_cnt_ena          <= '0';-- pkt_rt counter disable
                --
                fsm_state_next <= STANDBY;

        end case;
    end process p_fsm_comb;

    ------------
    -- STATUS --
    ------------
    status_err_seq      <= '1' when fsm_state = ERR_SEQ else '0';
    status_err_timeout  <= '1' when fsm_state = ERR_TO else '0';
    status_frame_count <= std_logic_vector(frame_counter);


end architecture rtl;
