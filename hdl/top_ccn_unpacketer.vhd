-- COMMUNICATION CELL NODE - Packet layer
-- UNPACKETER: from CCN Link layer frame to individual packet

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library desyrdl;
use desyrdl.common.all;
use desyrdl.pkg_ccn_unpacketer.all;

use work.pkg_ccn_packet_version.all;

entity top_ccn_unpacketer is
    generic(
        G_S_TDATA_W                 : positive;
        G_S_TUSER_W                 : positive;
        G_M_TDATA_W                 : positive;
        G_M_TUSER_W                 : positive;
        G_PROTOCOL_ID               : positive
    );
    port(
        -- Asynchronous reset
        aresetn                     : in std_logic;

        -- Running timeref
        timeref                     : in std_logic_vector(63 downto 0);

        -- AXI-MM Status and Config
        aclk                        : in std_logic;
        s_axi_m2s                   : in t_ccn_unpacketer_m2s;
        s_axi_s2m                   : out t_ccn_unpacketer_s2m;

        -- AXIS Frame input
        s_axis_clk                  : in std_logic; -- Only S_AXIS signals on this clk domain
        s_axis_tdata                : in std_logic_vector(G_S_TDATA_W-1 downto 0);
        s_axis_tkeep                : in std_logic_vector(G_S_TDATA_W/8-1 downto 0);
        s_axis_tlast                : in std_logic;
        s_axis_tvalid               : in std_logic;
        s_axis_tready               : out std_logic;
        s_axis_tuser                : in std_logic_vector(G_S_TUSER_W-1 downto 0);

        -- AXIS Packet output
        m_axis_tdata                : out std_logic_vector(G_M_TDATA_W-1 downto 0);
        m_axis_tuser                : out std_logic_vector(G_M_TUSER_W-1 downto 0);
        m_axis_tvalid               : out std_logic;
        m_axis_tready               : in std_logic;
        m_axis_tlast                : out std_logic

    );
end entity top_ccn_unpacketer;

architecture struct of top_ccn_unpacketer is

    -- Attributes for Xilinx Vivado Block Design
    ATTRIBUTE X_INTERFACE_INFO : STRING;
    ATTRIBUTE X_INTERFACE_INFO of s_axis_clk: SIGNAL is "xilinx.com:signal:clock:1.0 s_axis_clk CLK";
    ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
    ATTRIBUTE X_INTERFACE_PARAMETER of s_axis_clk: SIGNAL is "ASSOCIATED_BUSIF s_axis";

    ------------------------
    -- SIGNAL DECLARATION --
    ------------------------
    signal mac_dst             : std_logic_vector(47 downto 0);
    signal mac_src             : std_logic_vector(47 downto 0);

    -- AXIMM
    signal addrmap_i           : t_addrmap_ccn_unpacketer_in;
    signal addrmap_o           : t_addrmap_ccn_unpacketer_out;

    signal areset              : std_logic;


    -- Error flags
    signal f_err_pid           : std_logic;
    signal f_err_mac_len       : std_logic;
    signal f_err_mac_dst       : std_logic;
    signal f_err_mac_src       : std_logic;
    signal s_err_pid           : std_logic;
    signal s_err_mac_len       : std_logic;
    signal s_err_mac_dst       : std_logic;
    signal s_err_mac_src       : std_logic;

begin

    areset  <= not aresetn;

    ----------------------
    -- AXI-MM INTERFACE --
    ----------------------
    inst_aximm: entity desyrdl.ccn_unpacketer
    port map(
        pi_clock    => aclk,
        pi_reset    => areset,
        pi_s_top    => s_axi_m2s,
        po_s_top    => s_axi_s2m,
        pi_addrmap  => addrmap_i,
        po_addrmap  => addrmap_o
    );

    -- Mapping registers
    addrmap_i.version.data.data <= C_VERSION;
    mac_dst  <= addrmap_o.mac_dst_msb.data.data & addrmap_o.mac_dst_lsb.data.data;
    mac_src  <= addrmap_o.mac_src_msb.data.data & addrmap_o.mac_src_lsb.data.data;


    ----------
    -- CORE --
    ----------
    inst_core: entity work.ccn_unpack
    generic MAP(
        G_S_TDATA_W          => G_S_TDATA_W,
        G_S_TUSER_W          => G_S_TUSER_W,
        G_M_TDATA_W          => G_M_TDATA_W,
        G_M_TUSER_W          => G_M_TUSER_W,
        G_PROTOCOL_ID        => std_logic_vector(to_unsigned(G_PROTOCOL_ID, 8))
    )
    port map(
        rstn                  => aresetn,

        -- System input
        pps                   => '0',

        -- Control
        enable                => addrmap_o.control.enable.data(0),
        mac_len               => addrmap_o.mac_length.length.data,
        mac_dst               => mac_dst,
        mac_src               => mac_src,

        -- Status
        err_pid               => s_err_pid,
        err_mac_len           => s_err_mac_len,
        err_mac_dst           => s_err_mac_dst,
        err_mac_src           => s_err_mac_src,
        pkt_mcts              => open,
        frame_counter         => addrmap_i.frame_cnt.data.data,

        -- AXIS Frame input
        s_axis_clk            => s_axis_clk,
        s_axis_tdata          => s_axis_tdata,
        s_axis_tkeep          => s_axis_tkeep,
        s_axis_tlast          => s_axis_tlast,
        s_axis_tvalid         => s_axis_tvalid,
        s_axis_tready         => s_axis_tready,
        s_axis_tuser          => s_axis_tuser,

        -- AXIS Packet output
        m_axis_clk            => aclk,
        m_axis_tdata          => m_axis_tdata,
        m_axis_tuser          => m_axis_tuser,
        m_axis_tvalid         => m_axis_tvalid,
        m_axis_tready         => m_axis_tready,
        m_axis_tlast          => m_axis_tlast

    );

    ------------------
    -- STICKY FLAGS --
    ------------------
    addrmap_i.unpack_error.err_pid.data(0)      <= f_err_pid;
    addrmap_i.unpack_error.err_mac_len.data(0)  <= f_err_mac_len;
    addrmap_i.unpack_error.err_mac_dst.data(0)  <= f_err_mac_dst;
    addrmap_i.unpack_error.err_mac_src.data(0)  <= f_err_mac_src;

    p_flags:process(aclk, aresetn)
    begin
        if aresetn = '0' then
            f_err_pid           <= '0';
            f_err_mac_len       <= '0';
            f_err_mac_dst       <= '0';
            f_err_mac_src       <= '0';
        elsif rising_edge(aclk) then

            if addrmap_o.reset_error.reset.data(0) = '1' then
                f_err_pid           <= '0';
                f_err_mac_len       <= '0';
                f_err_mac_dst       <= '0';
                f_err_mac_src       <= '0';
            else

                if s_err_pid = '1' then
                    f_err_pid   <= '1';
                end if;

                if s_err_mac_len = '1' then
                    f_err_mac_len   <= '1';
                end if;

                if s_err_mac_dst = '1' then
                    f_err_mac_dst   <= '1';
                end if;

                if s_err_mac_src = '1' then
                    f_err_mac_src   <= '1';
                end if;
            end if;

        end if;
    end process;


end architecture struct;
