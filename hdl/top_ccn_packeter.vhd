-- COMMUNICATION CELL NODE - Packet layer
-- PACKETER: from individual packet to CCN link layer frame

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library desyrdl;
use desyrdl.common.all;
use desyrdl.pkg_ccn_packeter.all;

use work.pkg_ccn_packet_version.all;

entity top_ccn_packeter is
    generic(
        G_S_TDATA_W                 : positive;
        G_S_TUSER_W                 : positive;
        G_M_TUSER_W                 : positive;
        G_M_TDATA_W                 : positive;
        G_PROTOCOL_ID               : positive
    );
    port(
        -- Asynchronous reset
        aresetn                     : in std_logic;

        -- Running timeref
        timeref                     : in std_logic_vector(63 downto 0);

        -- PPS input 
        pps                         : in std_logic;

        -- AXI-MM Status and Config
        aclk                        : in std_logic;
        s_axi_m2s                   : in t_ccn_packeter_m2s;
        s_axi_s2m                   : out t_ccn_packeter_s2m;

        -- AXIS Packet input
        s_axis_tdata                : in std_logic_vector(G_S_TDATA_W-1 downto 0);
        s_axis_tuser                : in std_logic_vector(G_S_TUSER_W-1 downto 0);
        s_axis_tvalid               : in std_logic;
        s_axis_tready               : out std_logic;

        -- AXIS Frame output
        m_axis_clk                  : in std_logic; -- Only M_AXIS signals on this clk domain
        m_axis_tdata                : out std_logic_vector(G_M_TDATA_W-1 downto 0);
        m_axis_tkeep                : out std_logic_vector(G_M_TDATA_W/8-1 downto 0);
        m_axis_tlast                : out std_logic;
        m_axis_tvalid               : out std_logic;
        m_axis_tready               : in std_logic;
        m_axis_tuser                : out std_logic_vector(G_M_TUSER_W-1 downto 0)

    );
end entity top_ccn_packeter;

architecture rtl of top_ccn_packeter is

    -- Attributes for Xilinx Vivado Block Design
    ATTRIBUTE X_INTERFACE_INFO : STRING;
    ATTRIBUTE X_INTERFACE_INFO of m_axis_clk: SIGNAL is "xilinx.com:signal:clock:1.0 m_axis_clk CLK";
    ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
    ATTRIBUTE X_INTERFACE_PARAMETER of m_axis_clk: SIGNAL is "ASSOCIATED_BUSIF m_axis";

    ------------------------
    -- SIGNAL DECLARATION --
    ------------------------
    signal mac_dst             : std_logic_vector(47 downto 0);
    signal mac_src             : std_logic_vector(47 downto 0);

    -- AXIMM
    signal addrmap_i           : t_addrmap_ccn_packeter_in;
    signal addrmap_o           : t_addrmap_ccn_packeter_out;

    signal areset              : std_logic;

    -- Flags
    signal s_err_timeout        : std_logic;
    signal f_err_timeout        : std_logic;
    signal s_err_seq            : std_logic;
    signal f_err_seq            : std_logic;

begin

    areset  <= not aresetn;

    ----------------------
    -- AXI-MM INTERFACE --
    ----------------------
    inst_aximm: entity desyrdl.ccn_packeter
    port map(
        pi_clock    => aclk,
        pi_reset    => areset,
        pi_s_top    => s_axi_m2s,
        po_s_top    => s_axi_s2m,
        pi_addrmap  => addrmap_i,
        po_addrmap  => addrmap_o
    );

    -- Mapping registers
    addrmap_i.version.data.data <= C_VERSION;
    mac_dst  <= addrmap_o.mac_dst_msb.data.data & addrmap_o.mac_dst_lsb.data.data;
    mac_src  <= addrmap_o.mac_src_msb.data.data & addrmap_o.mac_src_lsb.data.data;

    ----------
    -- CORE --
    ----------
    inst_core: entity work.ccn_pack
    generic MAP(
        G_S_TDATA_W          => G_S_TDATA_W,
        G_S_TUSER_W          => G_S_TUSER_W,
        G_M_TDATA_W          => G_M_TDATA_W,
        G_M_TUSER_W          => G_M_TUSER_W,
        G_PROTOCOL_ID        => std_logic_vector(to_unsigned(G_PROTOCOL_ID, 8))
    )
    port map(
        aclk                 => aclk,
        aresetn              => aresetn,
        pps                  => pps,
        -- Control 
        packeter_run         => addrmap_o.control.enable.data(0),
        mac_dst              => mac_dst,
        mac_src              => mac_src,
        mac_length           => addrmap_o.mac_length.data.data,
        packet_timeout       => addrmap_o.timeout.data.data,
        expect_pkt_cnt       => addrmap_o.npacket.data.data,
        timeref              => timeref,
        latch_seq            => addrmap_o.control.latchseq.data(0),
	    pkt_rate_cnt	     => addrmap_i.pps_packeter_rate.pps_pck_cnt.data(23 downto 0),

        -- Status
        status_err_seq       => s_err_seq,
        status_err_timeout   => s_err_timeout,
        status_frame_count   => addrmap_i.packeter_count.data.data,
        latched_seq1         => addrmap_i.latched_seq1.data.data,
        latched_seq2         => addrmap_i.latched_seq2.data.data,

        -- AXIS Packet input
        s_axis_tdata         => s_axis_tdata,
        s_axis_tvalid        => s_axis_tvalid,
        s_axis_tready        => s_axis_tready,
        s_axis_tuser         => s_axis_tuser,

        -- AXIS Frame output
        m_axis_clk           => m_axis_clk,
        m_axis_tvalid        => m_axis_tvalid,
        m_axis_tready        => m_axis_tready,
        m_axis_tdata         => m_axis_tdata,
        m_axis_tkeep         => m_axis_tkeep,
        m_axis_tlast         => m_axis_tlast,
        m_axis_tuser         => m_axis_tuser

    );

    addrmap_i.pps_packeter_rate.pps_pck_cnt.data(31 downto 24) <= (others => '0');

    ------------------
    -- STICKY FLAGS --
    ------------------
    addrmap_i.packeter_error.timeout.data(0)    <= f_err_timeout;
    addrmap_i.packeter_error.seq.data(0)        <= f_err_seq;

    p_flags:process(aclk, aresetn)
    begin
        if aresetn = '0' then
            f_err_timeout   <= '0';
            f_err_seq       <= '0';
        elsif rising_edge(aclk) then

            if addrmap_o.reset_error.reset.data(0) = '1' then
                f_err_timeout  <= '0';
                f_err_seq      <= '0';
            else

                if s_err_timeout = '1' then
                    f_err_timeout   <= '1';
                end if;

                if s_err_seq = '1' then
                    f_err_seq       <= '1';
                end if;

            end if;

        end if;
    end process;

end architecture;
