library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ccn_unpack is
    generic(
        G_S_TDATA_W           : positive;
        G_S_TUSER_W           : positive;
        G_M_TDATA_W           : positive;
        G_M_TUSER_W           : positive;
        G_PROTOCOL_ID         : std_logic_vector(7 downto 0)
    );
    port(
        rstn                  : in std_logic;

        -- System input
        pps                   : in std_logic;

        -- Control
        enable                : in std_logic;
        mac_len               : in std_logic_vector(15 downto 0);
        mac_dst               : in std_logic_vector(47 downto 0);
        mac_src               : in std_logic_vector(47 downto 0);

        -- Status
        err_pid               : out std_logic;
        err_mac_len           : out std_logic;
        err_mac_dst           : out std_logic;
        err_mac_src           : out std_logic;
        pkt_mcts              : out std_logic_vector(63 downto 0);
        frame_counter         : out std_logic_vector(31 downto 0);

        -- AXIS Frame input
        s_axis_clk            : in std_logic; -- Only S_AXIS signals on this clk domain
        s_axis_tdata          : in std_logic_vector(G_S_TDATA_W-1 downto 0);
        s_axis_tkeep          : in std_logic_vector(G_S_TDATA_W/8-1 downto 0);
        s_axis_tlast          : in std_logic;
        s_axis_tvalid         : in std_logic;
        s_axis_tready         : out std_logic;
        s_axis_tuser          : in std_logic_vector(G_S_TUSER_W-1 downto 0);

        -- AXIS Packet output
        m_axis_clk            : in std_logic;
        m_axis_tdata          : out std_logic_vector(G_M_TDATA_W-1 downto 0);
        m_axis_tkeep          : out std_logic_vector(G_M_TDATA_W/8-1 downto 0);
        m_axis_tuser          : out std_logic_vector(G_M_TUSER_W-1 downto 0);
        m_axis_tvalid         : out std_logic;
        m_axis_tready         : in std_logic;
        m_axis_tlast          : out std_logic

    );
end entity ccn_unpack;

architecture rtl of ccn_unpack is

    ----------------------
    -- TYPE DECLARATION --
    ----------------------
    type t_state is (WAIT_LAST, G_ONE, G_TWO, G_THREE, G_OTHERS);

    ------------------------
    -- SIGNAL DECLARATION --
    ------------------------
    signal fsm_packet_pass    : std_logic;
    signal fsm_last_packet    : std_logic;
    signal fsm_dump           : std_logic;
    signal fsm_state          : t_state;
    signal fsm_state_next     : t_state;

    signal fifo_tvalid        : std_logic;
    signal fifo_tready        : std_logic;
    signal fifo_tdata         : std_logic_vector(G_S_TDATA_W-1 downto 0);
    signal fifo_tkeep         : std_logic_vector(G_S_TDATA_W/8-1 downto 0);
    signal fifo_tlast         : std_logic;
    signal fifo_tuser         : std_logic_vector(0 downto 0);

    signal wconv_in_tvalid       : std_logic;
    signal wconv_out_tvalid       : std_logic;

    -- header retrieved
    signal header_mac_dst   : std_logic_vector(47 downto 0);
    signal header_mac_src   : std_logic_vector(47 downto 0);
    signal header_mac_len   : std_logic_vector(15 downto 0);
    signal header_pkt_id    : std_logic_vector(7 downto 0);
    signal header_pkt_mcts  : std_logic_vector(63 downto 0);
    signal header_framenum  : std_logic_vector(7 downto 0);

    -- Status and stats
    signal frame_cnt        : unsigned(31 downto 0);

begin

    frame_counter   <= std_logic_vector(frame_cnt);

    ----------------
    -- INPUT FIFO --
    ----------------
    -- Also does clock domain conversion
    inst_fifo: entity work.ccn_axis_fifo_frame
    port map(
        s_axis_aresetn => rstn,
        s_axis_aclk    => s_axis_clk,
        s_axis_tvalid  => s_axis_tvalid,
        s_axis_tready  => s_axis_tready,
        s_axis_tdata   => s_axis_tdata,
        s_axis_tkeep   => s_axis_tkeep,
        s_axis_tlast   => s_axis_tlast,
        s_axis_tuser   => s_axis_tuser,
        m_axis_aclk    => m_axis_clk,
        m_axis_tvalid  => fifo_tvalid,
        m_axis_tready  => fifo_tready,
        m_axis_tdata   => fifo_tdata,
        m_axis_tkeep   => fifo_tkeep,
        m_axis_tlast   => fifo_tlast,
        m_axis_tuser   => fifo_tuser
    );

    -----------
    -- WCONV --
    -----------
    inst_wconv: entity work.ccn_axis_wconv_frame_packet
    port map(
        aclk                => m_axis_clk,
        aresetn             => rstn,
        s_axis_tvalid       => wconv_in_tvalid,
        s_axis_tready       => fifo_tready,
        s_axis_tdata        => fifo_tdata,
        s_axis_tkeep        => fifo_tkeep,
        s_axis_tlast        => fifo_tlast,
        m_axis_tvalid       => wconv_out_tvalid,
        m_axis_tready       => m_axis_tready,
        m_axis_tdata        => m_axis_tdata,
        m_axis_tkeep        => m_axis_tkeep,
        m_axis_tlast        => open
    );
    m_axis_tvalid   <= wconv_out_tvalid;
    m_axis_tlast    <= wconv_out_tvalid;
    m_axis_tuser    <= std_logic_vector(header_framenum(m_axis_tuser'range));

    wconv_in_tvalid <= (not fsm_dump) and fifo_tvalid;

    ---------
    -- FSM --
    ---------
    fsm_last_packet <= fsm_packet_pass and fifo_tlast;
    fsm_packet_pass <= fifo_tvalid and fifo_tready;

    p_fsm_sync:process(m_axis_clk, rstn)
    begin
        if rstn = '0' then
            fsm_state   <= WAIT_LAST;
        elsif rising_edge(m_axis_clk) then
            fsm_state   <= fsm_state_next;
        end if;
    end process p_fsm_sync;

    p_fsm_comb:process(fsm_state, enable, fsm_packet_pass, fsm_last_packet)
    begin
        case fsm_state is
            when WAIT_LAST =>
                fsm_dump    <= '1';
                if (enable and fsm_last_packet) = '1' then
                    fsm_state_next  <= G_ONE;
                else
                    fsm_state_next  <= WAIT_LAST;
                end if;

            when G_ONE =>
                fsm_dump    <= '1';
                if fsm_packet_pass = '1' then
                    fsm_state_next  <= G_TWO;
                else
                    fsm_state_next  <= G_ONE;
                end if;

            when G_TWO =>
                fsm_dump    <= '1';
                if fsm_packet_pass = '1' then
                    fsm_state_next  <= G_THREE;
                else
                    fsm_state_next  <= G_TWO;
                end if;

            when G_THREE =>
                fsm_dump    <= '1';
                if fsm_packet_pass = '1' then
                    fsm_state_next  <= G_OTHERS;
                else
                    fsm_state_next  <= G_THREE;
                end if;

            when G_OTHERS =>
                fsm_dump    <= '0';
                if enable = '0' then
                    fsm_state_next  <= WAIT_LAST;
                else
                    if fsm_last_packet = '1' then
                        fsm_state_next  <= G_ONE;
                    else
                        fsm_state_next  <= G_OTHERS;
                    end if;
                end if;

            when others =>
                fsm_dump    <= '1';
                fsm_state_next  <= WAIT_LAST;

        end case;
    end process p_fsm_comb;

    --------------------------------
    -- HEADER RETRIEVAL AND STATS --
    --------------------------------
    pkt_mcts    <= header_pkt_mcts;

    p_header:process(m_axis_clk, rstn)
    begin
        if rstn = '0' then
            header_mac_dst   <= (others => '0');
            header_mac_src   <= (others => '0');
            header_mac_src   <= (others => '0');
            header_mac_len   <= (others => '0');
            header_pkt_id    <= (others => '0');
            frame_cnt        <= (others => '0');
            header_framenum  <= (others => '0');
            err_pid          <= '0';
            err_mac_dst      <= '0';
            err_mac_src      <= '0';
            err_mac_len      <= '0';

        elsif rising_edge(m_axis_clk) then

            if fifo_tvalid = '1' and fifo_tready = '1' then

                if fsm_state = G_ONE then
                    header_mac_dst(47 downto 40)    <= fifo_tdata(7 downto 0);
                    header_mac_dst(39 downto 32)    <= fifo_tdata(15 downto 8);
                    header_mac_dst(31 downto 24)    <= fifo_tdata(23 downto 16);
                    header_mac_dst(23 downto 16)    <= fifo_tdata(31 downto 24);
                    header_mac_dst(15 downto 8)    <= fifo_tdata(39 downto 32);
                    header_mac_dst(7 downto 0)    <= fifo_tdata(47 downto 40);
                    header_mac_src(47 downto 40)    <= fifo_tdata(55 downto 48);
                    header_mac_src(39 downto 32)    <= fifo_tdata(63 downto 56);
                end if;

                if fsm_state = G_TWO then
                    header_mac_src(31 downto 24)    <= fifo_tdata(7 downto 0);
                    header_mac_src(23 downto 16)    <= fifo_tdata(15 downto 8);
                    header_mac_src(15 downto 8)     <= fifo_tdata(23 downto 16);
                    header_mac_src(7 downto 0)      <= fifo_tdata(31 downto 24);
                    header_mac_len(15 downto 8)     <= fifo_tdata(39 downto 32);
                    header_mac_len(7 downto 0)      <= fifo_tdata(47 downto 40);
                    header_pkt_id                   <= fifo_tdata(55 downto 48);
                    header_framenum                 <= fifo_tdata(63 downto 56);
                end if;

                if fsm_state = G_THREE then
                    frame_cnt <= frame_cnt + 1;

                    header_pkt_mcts <= fifo_tdata;

                    if header_pkt_id = G_PROTOCOL_ID then
                        err_pid <= '0';
                    else
                        err_pid <= '1';
                    end if;

                    if header_mac_dst  = mac_dst then
                        err_mac_dst <= '0';
                    else
                        err_mac_dst <= '1';
                    end if;

                    if header_mac_src  = mac_src then
                        err_mac_src <= '0';
                    else
                        err_mac_src <= '1';
                    end if;

                    if header_mac_len  = mac_len then
                        err_mac_len <= '0';
                    else
                        err_mac_len <= '1';
                    end if;
                end if;
            end if;


        end if;
    end process p_header;



end architecture rtl;
